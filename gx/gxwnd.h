/**
 * @file	gxwnd.h
 * @brief	GX ウィンドウ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "gx.h"
#include <memory.h>
#include "misc/taskbarwnd.h"

#if defined(SIZE_QVGA)
enum
{
	WINDOW_WIDTH	= 320,
	WINDOW_HEIGHT	= 240
};
#endif	// SIZE_QVGA
#if defined(SIZE_VGA)
enum
{
	WINDOW_WIDTH	= 640,
	WINDOW_HEIGHT	= 480
};
#endif	// SIZE_VGA

#ifdef STRICT
#define	SUBCLASSPROC	WNDPROC			/*!< プロシージャ型 */
#else
#define	SUBCLASSPROC	FARPROC			/*!< プロシージャ型 */
#endif

/**
 * @brief GX ウィンドウ クラス
 */
class GxWnd
{
protected:
	/**
	 * フラグ
	 */
	enum
	{
		GX_INIT		= 0x01,
		GX_WINDOW	= 0x02
	};

	/**
	 * @brief ビットマップ情報
	 */
	struct BMPINFO
	{
		BITMAPINFOHEADER bmiHeader;		/*!< ヘッダ */
		DWORD bmiColors[4];				/*!< 色 */
	};

	HWND m_hWnd;				/*!< ウィンドウ ハンドル */
	DWORD m_dwFlag;				/*!< フラグ */
	int m_nStartX;				/*!< 表示開始 X */
	int m_nStartY;				/*!< 表示開始 Y */
	DWORD m_dwStyle;			/*!< ウィンドウ スタイル退避 */
	DWORD m_dwStyleEx;			/*!< ウィンドウ 拡張スタイル退避 */
	HBITMAP m_hBitmap;			/*!< ビットマップ ハンドル */
	void* m_pImage;				/*!< イメージ */
	SUBCLASSPROC m_fnOld;		/*!< ウィンドウ プロシージャ退避 */
	GXDisplayProperties m_gxdp;	/*!< プロパティ */

	static GxWnd sm_instance;	/*!< 唯一のインスタンスです */

	/**
	 * 初期化
	 */
	void Init()
	{
		if (m_dwFlag)
		{
			return;
		}
		m_dwFlag = GX_INIT;
		DWORD cx = GetSystemMetrics(SM_CXSCREEN);
		DWORD cy = GetSystemMetrics(SM_CYSCREEN);

		int sx = 0;
		int sy = 0;

#if defined(SIZE_QVGA) || defined(SIZE_VGA)
		if ((cx > WINDOW_WIDTH) && (cy > WINDOW_HEIGHT))
		{
			m_dwFlag |= GX_WINDOW;
			cx = WINDOW_WIDTH;
			cy = WINDOW_HEIGHT;
		}
		else
		{
			if ((cx >= WINDOW_WIDTH) && (cy >= WINDOW_HEIGHT))
			{
				sx = (cx - WINDOW_WIDTH) / 2;
				sy = (cy - WINDOW_HEIGHT) / 2;
				cx = WINDOW_WIDTH;
				cy = WINDOW_HEIGHT;
			}
		}
#endif	// defined(SIZE_QVGA) || defined(SIZE_VGA)

		GXDisplayProperties dp;
		dp.cxWidth = cx;
		dp.cyHeight = cy;
		dp.cbxPitch = 2;
		dp.cbyPitch = ((dp.cxWidth * 2) + 3) & (~3);
		dp.cBPP = 16;
		dp.ffFormat = kfDirect | kfDirect565;
		m_gxdp = dp;

		m_nStartX = sx;
		m_nStartY = sy;
	}

	/**
	 * プロシージャ
	 * @param[in] hWnd ウィンドウへのハンドル
	 * @param[in] uMsg メッセージ
	 * @param[in] wParam 追加のメッセージ情報
	 * @param[in] lParam 追加のメッセージ情報
	 * @return リザルト コード
	 */
	static LRESULT CALLBACK OnMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		GxWnd* pWnd = GetInstance();

		switch (uMsg)
		{
			case WM_MOUSEMOVE:
			case WM_LBUTTONDBLCLK:
			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_RBUTTONDBLCLK:
			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			{
				const int x = LOWORD(lParam) - pWnd->m_nStartX;
				const int y = HIWORD(lParam) - pWnd->m_nStartY;
				return CallWindowProc(pWnd->m_fnOld, hWnd, uMsg, wParam, MAKELPARAM(x, y));
			}
		}
		return CallWindowProc(pWnd->m_fnOld, hWnd, uMsg, wParam, lParam);
	}

	/**
	 * クライアント サイズの設定
	 * @param[in] hWnd ウィンドウ
	 * @param[in] nWidth 幅
	 * @param[in] nHeight 高さ
	 */
	static void SetClientSize(HWND hWnd, LONG nWidth, LONG nHeight)
	{
		RECT rectWindow;
		GetWindowRect(hWnd, &rectWindow);
		RECT rectClient;
		GetClientRect(hWnd, &rectClient);
		const int w = nWidth + (rectWindow.right - rectWindow.left) - (rectClient.right - rectClient.left);
		const int h = nHeight + (rectWindow.bottom - rectWindow.top) - (rectClient.bottom - rectClient.top);

		const int scx = GetSystemMetrics(SM_CXSCREEN);
		const int scy = GetSystemMetrics(SM_CYSCREEN);
		RECT rectDesktop;
		SystemParametersInfo(SPI_GETWORKAREA, 0, &rectDesktop, 0);

		int x = rectWindow.left;
		int y = rectWindow.top;

		if (scx < w)
		{
			x = (scx - w) / 2;
		}
		else
		{
			if ((x + w) > rectDesktop.right)
			{
				x = rectDesktop.right - w;
			}
			if (x < rectDesktop.left)
			{
				x = rectDesktop.left;
			}
		}
		if (scy < h)
		{
			y = (scy - h) / 2;
		}
		else
		{
			if ((y + h) > rectDesktop.bottom)
			{
				y = rectDesktop.bottom - h;
			}
			if (y < rectDesktop.top)
			{
				y = rectDesktop.top;
			}
		}
		MoveWindow(hWnd, x, y, w, h, TRUE);
	}

public:
	/**
	 * インスタンスを得る
	 * @return インスタンス
	 */
	static GxWnd* GetInstance()
	{
		return &sm_instance;
	}

	/**
	 * コンストラクタ
	 */
	GxWnd()
	{
		ZeroMemory(this, sizeof(*this));
	}

	/**
	 * ディスプレイを開く
	 * @param[in] hWnd ウィンドウ ハンドル
	 * @param[in] dwFlags フラグ
	 * @retval 1 成功
	 * @retval 0 失敗
	 */
	int OpenDisplay(HWND hWnd, DWORD dwFlags)
	{
		if (m_hWnd)
		{
			return 0;
		}
		Init();

		// bitmap作成。
		BMPINFO bi;
		ZeroMemory(&bi, sizeof(bi));
		bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bi.bmiHeader.biWidth = m_gxdp.cxWidth;
		bi.bmiHeader.biHeight = 0 - m_gxdp.cyHeight;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biBitCount = 16;
		bi.bmiHeader.biCompression = BI_BITFIELDS;
		bi.bmiColors[0] = 0xf800;
		bi.bmiColors[1] = 0x07e0;
		bi.bmiColors[2] = 0x001f;
		{
			HDC hdc = GetDC(NULL);
			m_hBitmap = CreateDIBSection(hdc, reinterpret_cast<BITMAPINFO*>(&bi), DIB_RGB_COLORS, &m_pImage, NULL, 0);
			ReleaseDC(NULL, hdc);
		}
		if (m_hBitmap == NULL)
		{
			CloseDisplay();
			return 0;
		}
		ZeroMemory(m_pImage, m_gxdp.cbyPitch * m_gxdp.cyHeight);

		m_hWnd = hWnd;

		m_fnOld = reinterpret_cast<SUBCLASSPROC>(GetWindowLongPtr(hWnd, GWLP_WNDPROC));
		SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(OnMessage));

		if (m_dwFlag & GX_WINDOW)
		{
			SetClientSize(hWnd, m_gxdp.cxWidth, m_gxdp.cyHeight);
		}
		else
		{
			TaskBarWnd::Show(SW_HIDE);
			DWORD winstyle = GetWindowLong(hWnd, GWL_STYLE);
			m_dwStyle = winstyle;
			DWORD winstyleex = GetWindowLong(hWnd, GWL_EXSTYLE);
			m_dwStyleEx = winstyleex;
			winstyle &= ~(WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU);
			winstyle |= WS_POPUP | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
			winstyleex |= WS_EX_TOPMOST;
			SetWindowLong(hWnd, GWL_STYLE, winstyle);
			SetWindowLong(hWnd, GWL_EXSTYLE, winstyleex);
			SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_DRAWFRAME | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
			MoveWindow(hWnd, 0, 0, m_gxdp.cxWidth, m_gxdp.cyHeight, TRUE);
		}
		return 1;
	}

	/**
	 * ディスプレイを閉じる
	 * @retval 1 成功
	 */
	int CloseDisplay()
	{
		if (m_hBitmap)
		{
			m_pImage = NULL;
			DeleteObject(m_hBitmap);
			m_hBitmap = NULL;
		}
		if (m_hWnd)
		{
			SetWindowLongPtr(m_hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(m_fnOld));
			if (!(m_dwFlag & GX_WINDOW))
			{
				SetWindowLong(m_hWnd, GWL_STYLE, m_dwStyle);
				SetWindowLong(m_hWnd, GWL_EXSTYLE, m_dwStyleEx);
				SetWindowPos(m_hWnd, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				TaskBarWnd::Show(SW_SHOW);
			}
			m_hWnd = NULL;
		}
		return 0;
	}

	/**
	 * 描画開始
	 * @return ポインタ
	 */
	void* BeginDraw()
	{
		return m_pImage;
	}

	/**
	 * 描画終了
	 * @retval 1 成功
	 */
	int EndDraw()
	{
		if (m_hWnd)
		{
			HDC hdc = GetDC(m_hWnd);
			HDC hmemdc = CreateCompatibleDC(hdc);
			HBITMAP hbitmap = static_cast<HBITMAP>(SelectObject(hmemdc, m_hBitmap));
			BitBlt(hdc, m_nStartX, m_nStartY, m_gxdp.cxWidth, m_gxdp.cyHeight, hmemdc, 0, 0, SRCCOPY);
			SelectObject(hmemdc, hbitmap);
			DeleteDC(hmemdc);
			ReleaseDC(m_hWnd, hdc);
		}
		return 1;
	}

	/**
	 * ディスプレイ プロパティの取得
	 * @return プロパティ
	 */
	GXDisplayProperties GetDisplayProperties()
	{
		Init();
		return m_gxdp;
	}

	/**
	 * サスペンド
	 * @retval 1 成功
	 * @retval 0 失敗
	 */
	int Suspend()
	{
		if (m_hWnd)
		{
			if (!(m_dwFlag & GX_WINDOW))
			{
				TaskBarWnd::Show(SW_SHOW);
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * レジューム
	 * @retval 1 成功
	 * @retval 0 失敗
	 */
	int Resume()
	{
		if (m_hWnd)
		{
			if (!(m_dwFlag & GX_WINDOW))
			{
				TaskBarWnd::Show(SW_HIDE);
				MoveWindow(m_hWnd, 0, 0, m_gxdp.cxWidth, m_gxdp.cyHeight, TRUE);
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
};
