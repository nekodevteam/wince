/**
 * @file	gx.cpp
 * @brief	Defines the entry point for the DLL application.
 */

#include "stdafx.h"
#include "gx.h"
#include "gxwnd.h"

/*! 唯一のインスタンスです */
GxWnd GxWnd::sm_instance;

/**
 * ダイナミックリンクライブラリ（DLL）のオプションのエントリポイントです
 * @param[in] hModule DLL モジュールのハンドルを指定します
 * @param[in] ul_reason_for_call DLL エントリポイント関数が呼び出された原因を示すフラグを指定します
 * @param[in] lpReserved DLL の初期化およびクリーンアップの方法を表す値を指定します
 * @return 関数が成功すると TRUE、関数が初期化に失敗すると FALSE が返ります
 */
BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

/**
 * ディスプレイを開く
 * @param[in] hWnd ウィンドウ ハンドル
 * @param[in] dwFlags フラグ
 * @retval 1 成功
 * @retval 0 失敗
 */
int GXOpenDisplay(HWND hWnd, DWORD dwFlags)
{
	return GxWnd::GetInstance()->OpenDisplay(hWnd, dwFlags);
}

/**
 * ディスプレイを閉じる
 * @retval 1 成功
 */
int GXCloseDisplay(void)
{
	return GxWnd::GetInstance()->CloseDisplay();
}

/**
 * 描画開始
 * @return ポインタ
 */
void* GXBeginDraw(void)
{
	return GxWnd::GetInstance()->BeginDraw();
}

/**
 * 描画終了
 * @retval 1 成功
 */
int GXEndDraw(void)
{
	return GxWnd::GetInstance()->EndDraw();
}

/**
 * ディスプレイ プロパティの取得
 * @return プロパティ
 */
GXDisplayProperties GXGetDisplayProperties(void)
{
	return GxWnd::GetInstance()->GetDisplayProperties();
}

/**
 * サスペンド
 * @retval 1 成功
 * @retval 0 失敗
 */
int GXSuspend(void)
{
	return GxWnd::GetInstance()->Suspend();
}

/**
 * レジューム
 * @retval 1 成功
 * @retval 0 失敗
 */
int GXResume(void)
{
	return GxWnd::GetInstance()->Resume();
}

/**
 * 入力開始
 * @retval 1 成功
 */
int GXOpenInput(void)
{
	return 1;
}

/**
 * 入力終了
 * @retval 1 成功
 */
int GXCloseInput(void)
{
	return 1;
}

/**
 * キー リストを得る
 * @param[in] iOptions オプション
 * @return キー リスト
 */
GXKeyList GXGetDefaultKeys(int iOptions)
{
	/* キー リスト */
	static const GXKeyList gxkl =
	{
		VK_UP,		{0, 0},
		VK_DOWN, 	{0, 0},
		VK_LEFT,	{0, 0},
		VK_RIGHT,	{0, 0},
		VK_RETURN,	{0, 0},
		VK_ESCAPE,	{0, 0},
		'Z',		{0, 0},
		'X',		{0, 0}
	};
	return gxkl;
}
