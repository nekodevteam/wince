/**
 * @file	taskbarwnd.h
 * @brief	タスク バー ウィンドウ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

/**
 * @brief タスク バー ウィンドウ クラス
 */
class TaskBarWnd
{
public:
	/**
	 * タスク バーのウィンドウ ハンドルを得る
	 * @return ウィンドウ ハンドル
	 */
	inline static HWND Handle()
	{
#if defined(_WIN32_WCE)
		static const TCHAR szTaskBarClass[] = TEXT("HHTaskBar");				/*!< タスク バー名 */
#else
		static const TCHAR szTaskBarClass[] = TEXT("Shell_TrayWnd");			/*!< タスク バー名 */
#endif
		return FindWindow(szTaskBarClass, NULL);
	}

	/**
	 * タスク バーの表示
	 * @param[in] nCmdShow ウィンドウの表示方法を制御します
	 */
	inline static void Show(int nCmdShow)
	{
		HWND hTaskBar = Handle();
		if (hTaskBar)
		{
			ShowWindow(hTaskBar, nCmdShow);
		}
	}
};
