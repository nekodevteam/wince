/**
 * @file	gx.h
 * @brief	Interface of the GAPI
 */

#pragma once

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GX_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GX_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef GX_EXPORTS
#define GX_API __declspec(dllexport)		/*!< エクスポート */
#else
#define GX_API __declspec(dllimport)		/*!< インポート */
#endif

/**
 * フラグ
 */
enum
{
	GX_FULLSCREEN		= 0x01,
	GX_NORMALKEYS		= 0x02,
};

/**
 * 画面モード
 */
enum
{
	kfLandscape			= 0x008,
	kfPalette			= 0x010,
	kfDirect			= 0x020,
	kfDirect555			= 0x040,
	kfDirect565			= 0x080,
	kfDirect888			= 0x100,
	kfDirect444			= 0x200,
	kfDirectInverted	= 0x400
};

/**
 * @brief プロパティ
 */
struct GXDisplayProperties
{
	DWORD	cxWidth;			/*!< 幅 */
	DWORD	cyHeight;			/*!< 高さ */
	long	cbxPitch;			/*!< X ピッチ */
	long	cbyPitch;			/*!< Y ピッチ */
	long	cBPP;				/*!< 色数 */
	DWORD	ffFormat;			/*!< フォーマット */
};

/**
 * @brief キー リスト
 */
struct GXKeyList
{
	short	vkUp;				/*!< 上ボタン コード */
	POINT	ptUp;				/*!< 上ボタン 位置 */
	short	vkDown;				/*!< 下ボタン コード */
	POINT	ptDown;				/*!< 下ボタン 位置 */
	short	vkLeft;				/*!< 左ボタン コード */
	POINT	ptLeft;				/*!< 左ボタン 位置 */
	short	vkRight;			/*!< 右ボタン コード */
	POINT	ptRight;			/*!< 右ボタン 位置 */
	short	vkA;				/*!< A ボタン コード */
	POINT	ptA;				/*!< A ボタン 位置 */
	short	vkB;				/*!< B ボタン コード */
	POINT	ptB;				/*!< B ボタン 位置 */
	short	vkC;				/*!< C ボタン コード */
	POINT	ptC;				/*!< C ボタン 位置 */
	short	vkStart;			/*!< スタート ボタン コード */
	POINT	ptStart;			/*!< スタート ボタン 位置 */
};

GX_API int GXOpenDisplay(HWND hWnd, DWORD dwFlags);
GX_API int GXCloseDisplay(void);
GX_API void* GXBeginDraw(void);
GX_API int GXEndDraw(void);
GX_API GXDisplayProperties GXGetDisplayProperties(void);
GX_API int GXSuspend(void);
GX_API int GXResume(void);

GX_API int GXOpenInput(void);
GX_API int GXCloseInput(void);
GX_API GXKeyList GXGetDefaultKeys(int iOptions);
