/**
 * @file	keyscan.cpp
 * @brief	Defines the entry point for the application.
 */

#include "stdafx.h"
#include "resource.h"
#include "vk.h"

#define MAX_LOADSTRING 100							/*!< バッファ サイズ */

// グローバル変数:
static HINSTANCE hInst;								/*!< 現在のインスタンス */
static TCHAR szTitle[MAX_LOADSTRING];				/*!< タイトル バー テキスト */
static TCHAR szWindowClass[MAX_LOADSTRING];			/*!< クラス名 */

// このコード モジュールに含まれる関数の前宣言:
static ATOM MyRegisterClass(HINSTANCE hInstance);
static BOOL InitInstance(HINSTANCE, int);
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
static LRESULT CALLBACK About(HWND, UINT, WPARAM, LPARAM);

static TCHAR s_szLine[16][256];			/*!< 表示バッファ */
static unsigned int s_nLineIndex;		/*!< 表示インデックス */

/**
 * キー ダウン イベント
 * @param[in] hWnd ウィンドウ ハンドル
 * @param[in] message 処理される Windows メッセージを指定します
 * @param[in] wParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @param[in] lParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @return メッセージに依存する値を返します
 */
static LRESULT OnKeyDown(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	wsprintf(s_szLine[s_nLineIndex], TEXT("%04d %04X %04X %04X %04X %s"), message, HIWORD(wParam), LOWORD(wParam), HIWORD(lParam), LOWORD(lParam), VKGetName(LOWORD(wParam)));
	s_nLineIndex = (s_nLineIndex + 1) % 16;
	InvalidateRect(hWnd, NULL, TRUE);
	return 0;
}

/**
 * 描画
 * @param[in] hdc デバイス コンテキスト
 */
static void Draw(HDC hdc)
{
	for (unsigned int i = 0; i < 16; i++)
	{
		LPCTSTR str = s_szLine[(s_nLineIndex + 15 - i) % 16];
		ExtTextOut(hdc, 0, i * 20, ETO_OPAQUE, NULL, str, lstrlen(str), NULL);
	}
}

/**
 * アプリケーションの初期エントリポイント
 * @param[in] hInstance 現在のインスタンスのハンドル
 * @param[in] hPrevInstance 前のインスタンスのハンドル
 * @param[in] lpCmdLine コマンドライン
 * @param[in] nCmdShow ウィンドウの表示状態
 * @return 終了コード
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	// TODO: Place code here.

	// グローバル ストリングを初期化します
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_KEYSCAN, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// アプリケーションの初期化を行います:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_KEYSCAN);

	// メイン メッセージ ループ:
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator (msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}

/**
 * ウィンドウ クラスの登録
 * @note この関数およびその使用はこのコードを Windows 95 で先に追加された
 *       'RegisterClassEx' 関数と Win32 システムの互換性を保持したい場合に
 *       のみ必要となります。アプリケーションが、アプリケーションに関連付け
 *       られたスモール アイコンを取得できるよう、この関数を呼び出すことは
 *       重要です。
 * @param[in] hInstance インスタンス
 * @return 登録されているクラスを一意に識別するクラスアトムです
 */
static ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASS wc;

	wc.style			= CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= (WNDPROC)WndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= hInstance;
	wc.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_KEYSCAN));
#if defined(_WIN32_WCE)
	wc.hCursor			= 0;
	wc.hbrBackground	= (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName		= 0;
#else	// defined(_WIN32_WCE)
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName		= MAKEINTRESOURCE(IDC_KEYSCAN);
#endif	// defined(_WIN32_WCE)
	wc.lpszClassName	= szWindowClass;

	return RegisterClass(&wc);
}

/**
 * インスタンス ハンドルの保存とメイン ウィンドウの作成
 * @note この関数では、インスタンス ハンドルをグローバル変数に保存し、プログラムの
 *       メイン ウィンドウを作成し表示します。
 * @param[in] hInstance インスタンス
 * @param[in] nCmdShow ウィンドウの表示方法
 * @retval TRUE 成功
 * @retval FALSE 失敗
 */
static BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // グローバル変数にインスタンス ハンドルを保存します

#if defined(_WIN32_WCE)
	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
#else	// defined(_WIN32_WCE)
	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
#endif	// defined(_WIN32_WCE)

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

/**
 * メイン ウィンドウのメッセージを処理します。
 * @note WM_COMMAND - アプリケーション メニューの処理
 *       WM_PAINT   - メイン ウィンドウの描画
 *       WM_DESTROY - 終了メッセージの通知とリターン
 * @param[in] hWnd ウィンドウ ハンドル
 * @param[in] message 処理される Windows メッセージを指定します
 * @param[in] wParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @param[in] lParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @return メッセージに依存する値を返します
 */
static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_COMMAND:
			{
				const unsigned int wmId = LOWORD(wParam);
				// const unsigned int wmEvent = HIWORD(wParam);
				// メニュー選択の解析:
				switch (wmId)
				{
					case IDM_ABOUT:
						DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
						break;

					case IDM_EXIT:
						DestroyWindow(hWnd);
						break;

					default:
						return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
			break;

		case WM_PAINT:
			{
				PAINTSTRUCT ps;
				HDC hdc = BeginPaint(hWnd, &ps);
				// TODO: この位置に描画用のコードを追加してください...
				Draw(hdc);
				EndPaint(hWnd, &ps);
			}
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
#if 0
		case WM_KEYUP:
		case WM_SYSKEYUP:
#endif	// 0
			return OnKeyDown(hWnd, message, wParam, lParam);

#ifdef _WIN32_WCE
		case WM_LBUTTONDOWN:
			DestroyWindow(hWnd);
			break;
#endif	// _WIN32_WCE

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

/**
 * バージョン情報ボックス用メッセージ ハンドラ
 * @param[in] hDlg ダイアログ ハンドル
 * @param[in] message 処理される Windows メッセージを指定します
 * @param[in] wParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @param[in] lParam メッセージの処理で使う付加情報を提供します。このパラメータの値はメッセージに依存します
 * @return メッセージに依存する値を返します
 */
static LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			return TRUE;

		case WM_COMMAND:
			{
				const unsigned int wmId = LOWORD(wParam);
				if ((wmId == IDOK) || (wmId == IDCANCEL))
				{
					EndDialog(hDlg, wmId);
					return TRUE;
				}
			}
			break;
	}
	return FALSE;
}
