/**
 * @file	keyscan.h
 * @brief	Defines the entry point for the application.
 */

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
