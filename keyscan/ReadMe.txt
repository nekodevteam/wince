========================================================================
       Windows CE APPLICATION : keyscan
========================================================================


AppWizard has created this keyscan application for you.  

This file contains a summary of what you will find in each of the files that
make up your keyscan application.

keyscan.cpp
    This is the main application source file.

keyscan.vcp
    This file (the project file) contains information at the project level and
    is used to build a single project or subproject. Other users can share the
    project (.vcp) file, but they should export the makefiles locally.

/////////////////////////////////////////////////////////////////////////////
AppWizard は以下のリソースを作成しました:

keyscan.rc
    このファイルはプログラムが使用する Microsoft Windows のリソースを列挙しま
    す。このファイルは RES サブディレクトリに保存されているアイコン、ビットマ
    ップ、カーソルを含みます。このファイルは Microsoft Visual C++ で直接編集可能です。

res\keyscan.ico
    このファイルは、アプリケーションのアイコンとして使用されるアイコンファイル(32x32) です。
    このアイコンはリソース ファイル keyscan.rc によってインクルードされます。
small.ico
    %%これはスモール バージョン (16x16) のアイコンを含むアイコンファイルです。
    このアイコンはリソース ファイル keyscan.rc によってインクルードされます。
	

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named keyscan.pch and a precompiled types file named StdAfx.obj.

Resource.h
    このファイルは新規リソース ID を定義する標準ヘッダー ファイルです。
    Microsoft Visual C++ はこのファイルを読み込み更新します。

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.


/////////////////////////////////////////////////////////////////////////////
