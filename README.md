# Windows CE 関係
Windows CE 関係のソースコード置き場です。

## 開発環境
- Windows11 x64
- Microsoft eMbedded Visual C++ 3.0
  - Microsoft Windows SDK for Pocket PC 2002
  - [インストール方法](https://qiita.com/nekopro2/items/b78c256c84a54417c58a)

## 内容
- gx
  - GAPI v1.2 互換 DLL です
  - [詳細](https://qiita.com/nekopro2/items/9a44e8f28e52c1eb5b02)

- keyscan
  - Windows イベントである WM_KEYDOWN / WM_SYSKEYDOWN の内容を表示します
